# Temario capacitacion Linux + GIT + Ansible

### Objetivos del curso:
  - Objetivos Principales:
    - Adquirir un conocimiento basico de Linux con el cual te sientas comodo/a de moverse  por la consola
    - conocimientos necesarios para usar GIT como herramienta de control de cambios.
    - Lograr los conocimientos basicos para optimizar/ automatizar tareas.
  - Objetivos Secundarios:
    - Movete en la consola como pez en el agua.
    - Conocimientos minimos de troubleshooting
    - Infraestructura por codigo: Automatizacion con ansible / scripting / aprovisionamiento de equipos... etc
  - Objetivos Generales:
    - Que le pierdan el miedo a linux.. 
      Se sientan seguros al usarlo y dar un marco de herramientas como puntapie inicial para afrontar nuevos desafios.
      Contar con los conocimientos para afrontar gran parte de las tareas de automatizacion y trabajando con buenas practicas.

## Clase introduccion
  - [x] GNU / Linux
  - [ ] Virtualizando un linux (VirtualBox / vmware)
    - Instalacion Basica (verison Disney)
    - Levantar una vm osbox
    - Levantar vm vagrant (CI/CD)
  - [ ] Usando un linux desde la web (ssh/putty)
  - [ ] Estructura del SO. 

## Consola (Comandos basicos, skills)
  - [ ] El unico comando que tenes que saber (man)
  - [ ] Como moverse, donde estoy
  - [ ] visualializar archivos
    - cat
    - more , less
    - head , tail
  - [ ] bash (standar input, output, error)
  - [ ] Redireccionamiento (< > >> Creando archivos) 
```sh
cat << EOF > /tmp/un_archivo.txt
linea1
linea2
EOF

bc<<<5+2
grep $(whoami) /etc/shadow |awk -F ':' '{print $2}' > hash_de_mi_clave.txt

```
## Offtopic: GIT basico
  - [ x ] Usando repos
  - [ ] creando repos locales y asociandolos contra los remotos
  - [ ] Ramas

## Manejo de datos.
  - [ ] intruduccion a vim
  - [ ] filtrar informacion: grep | awk
  - [ ] xargs
  - [ ] scripting nivel babosa.

## Usuarios / permisos
  - [ ] ABM Usuarios
  - [ ] ABM grupos
  - [ ] Permisos
  - [ ] Archivos de configuracion relacionados

## Manejador de paquetes / repositorios
  - [ ] buscar, instalar, y conocer que esta instalado
  - [ ] agregando repositorios

## Ansible Intro:
  - [ ] Que es, para que sirve, como se usa
  - [ ] Mejores practicas
  - [ ] Estructura

## Ansible escribiendo recetas
  - [ ] Receta
  - [ ] Roles
  - [ ] Ansible Galaxy
  - [ ] Manejo de datos: filtros jinja

## Particionamiento & FS
  - [ ] Particionamiento de un disco / FS 
  - [ ] LVM

## Avanzadas I
  - [ ] Instalacion Avanzada (Como Dios Manda)
  - [ ] Tirando magia en la consola... 
    - grep, awk
    - vim
    - scripting avanzado

## Conectividad/Redes
  - [ ] curl, wget.
  - [ ] ssh
  - [ ] telnet, nc, ss
  - [ ] netstat
  - [ ] firewall (basico)

## Seguridad: Selinux / ACL
  - [ ] modos selinux
  - [ ] contextos.
  - [ ] ACL 


